import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Atendente from './src/Dashboard/Atendente';
import Lista from './src/views/beneficiario/Lista';
import Detalhes from './src/views/beneficiario/Detalhes';
import Formulario from './src/views/beneficiario/Formulario';
import Endereco from './src/views/endereco/Beneficiario';
import Login from './src/views/auth/Login'

import {Provider} from "react-redux";
import Store from "./src/redux/Store"


const Stack = createStackNavigator();


export default function App() {
  return (
    <Provider store={Store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login" component={Login}/>
          
          <Stack.Screen name="Atendente" component={Atendente}  />
          <Stack.Screen name="Lista" component={Lista} />
          <Stack.Screen name="Detalhes" component={Detalhes} />
          <Stack.Screen name="Formulario" component={Formulario} />
          <Stack.Screen name="Endereco" component={Endereco} />

        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
