import React from 'react';

// Componentes
import { StatusBar } from 'expo-status-bar';

import { StyleSheet, Text, View, Image } from 'react-native';
import Modal from 'modal-react-native-web'; // apenas para DEV
import { Button, Overlay, Icon  } from 'react-native-elements';

import Hr from "../../components/Endereco";

// redux
import { connect } from "react-redux";
import {buscarPessoaId, excluirPessoa} from "../../redux/actions/Beneficiario";
import {pessoaFilter} from "../../redux/filters/Beneficiario";

class Detalhes extends React.Component{
    
    constructor(props){
        super(props);

        const {id} = this.props.route.params;
        this.state = { 
            id: id,   
            modalVisible: false
        }

        this.props.navigation.setOptions({
            title: "Detalhes do Beneficiário",
            headerRight: () => (
                <View style={styles.container} >

                    <Button
                        type="clear"
                        icon={ <Icon name='create' size={30} type='ionicon' color='red' /> }
                        onPress={ () => this.onPressUpdate() }
                    />

                    <Button
                        type="clear"
                        icon={ <Icon name='trash' type='ionicon' size={30} color='red' /> }
                        onPress={ () => this.setModalVisible() }
                    />

                </View>
            ),

        });

    }

    onPressUpdate = () => {
        this.props.navigation.navigate('Formulario',{id: this.state.id });
    }
    onPressExcluir = () => {

        this.props.excluirPessoa(this.state.id);
        this.setModalVisible();
        this.props.navigation.navigate('Lista');
        
    }
 
    setModalVisible = () => {
        this.setState({ modalVisible: !this.state.modalVisible });
    }


    componentDidMount(){
        this.props.buscarPessoaId(this.state.id);
    }

    render(){

        return(
            <View >
                
                <View style={styles.container} >
                    <Image style={styles.userIcon} source={{ uri: this.props.pessoaItem.img }} />
                    <Text style={styles.userName} >{this.props.pessoaItem.name}</Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> Sexo: </Text>
                    <Text> {this.props.pessoaItem.sexo}  </Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> CPF: </Text>
                    <Text> {this.props.pessoaItem.cpf}  </Text>
                </View>
                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> Email: </Text>
                    <Text> {this.props.pessoaItem.email}  </Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> Telefone: </Text>
                    <Text> {this.props.pessoaItem.telefone}  </Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> Beneficio: </Text>
                    <Text> {this.props.pessoaItem.beneficio}  </Text>
                </View>

               
                <Hr text="Endereço" />

                <View style={styles.enderecoContainer}>
                    <Text style={styles.textBold}> Bairro: </Text>
                    <Text> {this.props.pessoaItem.bairro}  </Text>
                </View>
                    

                <View style={styles.enderecoContainer}>
                    <Text style={styles.textBold}> Av/Rua: </Text>
                    <Text> {this.props.pessoaItem.rua}  </Text>
                </View>

                <View style={styles.enderecoContainer}>
                    <Text style={styles.textBold}> Complemento: </Text>
                    <Text> {this.props.pessoaItem.complemento}  </Text>
                </View>
               

                <View style={styles.enderecoContainer}>
                    <Text style={styles.textBold}> Cidade: </Text>
                    <Text> {this.props.pessoaItem.cidade}  </Text>
                </View>

                <View style={styles.enderecoContainer}>
                    <Text style={styles.textBold}> Estado: </Text>
                    <Text> {this.props.pessoaItem.estado}  </Text>
                </View>


                <Overlay ModalComponent={Modal} isVisible={this.state.modalVisible} onBackdropPress={() => this.setModalVisible()}>
                    <Text style={styles.textHeader}>Confirmação !</Text>
                    <Text>Deseja excluir o {this.props.pessoaItem.name} ?</Text>

                    <View  style={styles.container}>
                        <Button
                            type="solid"
                            onPress={ () => this.setModalVisible() }
                            title="Cancelar"
                            buttonStyle={styles.btnSecondary}
                        />
                        <Button
                            type="solid"
                            onPress={ () => this.onPressExcluir() }
                            title="Excluir"
                            buttonStyle={styles.btnDanger}
                        />
                    </View>

                </Overlay>


            </View>
        )
    }

}


export default connect(pessoaFilter, {buscarPessoaId, excluirPessoa})(Detalhes)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1ecec',
        flexDirection: 'row',
        alignItems: "center",
        padding: 10
    },
    userIcon: {
        width: 80,
        height: 80,
        marginRight: 20
    },
    userName: {
        fontSize: 22,
    },
    textInfo: {
        margin:5,
        backgroundColor: '#f1ecec',
        width: '90%',
        fontSize: 17,
        borderRadius: 7,
        
        
    },
    textBold: {
        fontWeight: "bold",
        width: '30%',
        color: '#5686d6',
        fontSize: 17,
        borderRadius: 7,
        
    },
    textHeader: {
        fontWeight: "bold",
        fontSize: 22
    },

    btnDanger: {
        backgroundColor: "#dc3545",
        color: "#fff"
    },
    btnSecondary: {
        backgroundColor: "#6c757d",
        color: "#fff",
        marginRight: 20
    },
    
    

   

});