import React from 'react';

// Componentes
import { FlatList, SafeAreaView, View,  StyleSheet} from "react-native";
import { Button, Icon  } from 'react-native-elements';
import UserBeneficiario from "../../components/UserBeneficiario";

// Redux
import { connect } from "react-redux";
import {listarPessoa} from "../../redux/actions/Beneficiario";
import {pessoaFilter} from "../../redux/filters/Beneficiario";


class Lista extends React.Component{
    constructor(props){
        super(props);

        this.props.navigation.setOptions({
            title: "Lista dos Beneficiarios",
            headerRight: () => (
                <View style={styles.container} >

                     <Button
                        type="clear"
                        icon={ <Icon name='add-circle' type='ionicon' size={30} color='red' /> }
                        onPress={ () => this.onPressNew() }
                    />

                </View>
            ),

        });

    }

    onPressNew = () => {
        this.props.navigation.navigate('Formulario',{id: undefined });
    }

    componentDidMount(){
        this.props.listarPessoa();
        console.log("LISTA = ", this.props.pessoaLista);
    }

    render(){
        return(
            <SafeAreaView>
                <FlatList
                    data={this.props.pessoaLista}
                    renderItem={UserBeneficiario}
                    keyExtractor={(item) => item.id}
                />
            </SafeAreaView>
        )
    }

}

export default connect(pessoaFilter,{listarPessoa})(Lista);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1ecec',
        flexDirection: 'row',
        alignItems: "center",
        padding: 10
    }
});