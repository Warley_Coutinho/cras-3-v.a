import React from 'react';

// Componentes
import { SafeAreaView, View, StyleSheet, Text, TextInput } from "react-native";
import { Button } from 'react-native-elements';

// Formik
import { Formik } from 'formik';

// Redux
import { connect } from "react-redux";
import {salvarPessoa,clearPessoaItem} from "../../redux/actions/Beneficiario";
import {pessoaFilter} from "../../redux/filters/Beneficiario";

class Formulario extends React.Component{

    constructor(props){
        super(props);

        const {id} = this.props.route.params;
        if(id == undefined){
            this.props.clearPessoaItem();
        }

        
    }

    componentDidMount(){}

    handleSubmit = (pessoa) => {
        this.props.salvarPessoa(pessoa);
        this.props.navigation.navigate('Lista');
    }

    mapInitialValues = () => {
        return {
            id: this.props.pessoaItem.id || undefined,
            name: this.props.pessoaItem.name || '',
            cpf: this.props.pessoaItem.cpf || '',
            img: this.props.pessoaItem.img || '',
            sexo: this.props.pessoaItem.sexo || '',
            email: this.props.pessoaItem.email || '',
            telefone: this.props.pessoaItem.telefone || '',
            beneficio: this.props.pessoaItem.beneficio || '',
            bairro: this.props.pessoaItem.bairro || '',
            rua: this.props.pessoaItem.rua || '',
            complemento: this.props.pessoaItem.complemento || '',
            cidade: this.props.pessoaItem.cidade || '',
            estado: this.props.pessoaItem.estado || ''
            
            
        };
    }

    render(){
        return(
            <SafeAreaView>
                
                <Text style={styles.text}> Cadastrar Beneficiarios.</Text>
                
                
                <Formik
                    initialValues={ this.mapInitialValues()}
                    onSubmit={ (values) => this.handleSubmit(values) }
                    enableReinitialize
                >

                    { ({handleChange, handleSubmit,values}) => (

                        <View>

                            <TextInput  style={styles.input} label="Nome" type="requirede" placeholder='Nome: ' value={values.name} onChangeText={handleChange("name")} />
                            <TextInput  style={styles.input} label="Sexo" placeholder='Sexo:' value={values.sexo} onChangeText={handleChange("sexo")} />
                            <TextInput  style={styles.input} label="CPF" placeholder='CPF:' value={values.cpf} onChangeText={handleChange("cpf")} />
                            <TextInput  style={styles.input} label="Email" placeholder='Email:' value={values.email} onChangeText={handleChange("email")} />
                            <TextInput  style={styles.input} label="Telefone" placeholder='Telefone:' value={values.telefone} onChangeText={handleChange("telefone")} />
                            <TextInput  style={styles.input} label="Beneficio" placeholder='Beneficio:' value={values.beneficio} onChangeText={handleChange("beneficio")} />
                            <TextInput  style={styles.input} label="Bairro" placeholder='Bairro:' value={values.bairro} onChangeText={handleChange("bairro")} />
                            <TextInput  style={styles.input} label="Av/Rua" placeholder='Av/Rua:' value={values.rua} onChangeText={handleChange("rua")} />
                            <TextInput  style={styles.input} label="Complemento" placeholder='Complemento:' value={values.complemento} onChangeText={handleChange("complemento")} />
                            <TextInput  style={styles.input} label="Cidade" placeholder='Cidade:' value={values.cidade} onChangeText={handleChange("cidade")} />
                            <TextInput  style={styles.input} label="Estado" placeholder='Estado:' value={values.estado} onChangeText={handleChange("estado")} />
                           

                            <Button title="Salvar" onPress={handleSubmit} />

                        </View>

                    )}

                </Formik>

                        



            </SafeAreaView>
        )
    }

}

export default connect(pessoaFilter, {salvarPessoa,clearPessoaItem})(Formulario)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1ecec',
        flexDirection: 'row',
        alignItems: "center",
        padding: 10
    },
    userIcon: {
        width: 80,
        height: 80,
        marginRight: 20
    },
    userName: {
        fontSize: 22,
    },
    textInfo: {
        borderBottomColor: "#f1ecec",
        margin: 10
    },
    textHeader: {
        fontWeight: "bold",
        fontSize: 22,
    
    },
    text:{
       
        color:'#5686d6',
        fontSize: 20,
        
    },

    btnDanger: {
        backgroundColor: "#dc3545",
        color: "#fff"
    },
    btnSecondary: {
        backgroundColor: "#6c757d",
        color: "#fff",
        marginRight: 20
    },
    input:{
        backgroundColor: '#f1ecec',
        width: '100%',
        marginBottom: 15,
        color: '#222',
        fontSize: 17,
        borderRadius: 7,
        padding: 10
    }

});