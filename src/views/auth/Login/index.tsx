import React from 'react';
import { StyleSheet, TextInput, View, 
         KeyboardAvoidingView,Image,
         TouchableOpacity,Text } from 'react-native';

import { Button, Icon } from 'react-native-elements';



class Login extends React.Component{

    constructor(props, context){
        super(props, context);
    }

    render(){
        return(
           <KeyboardAvoidingView style={styles.background}>
               <View style={styles.containerLogo}>
                  <Image
                  style={{
                      width: 130,
                      height: 155,
                  }}
                  source={require('../../../../assets/img/LogoC.png')}
                  />
               </View>

               <View style={styles.container}>
                   <TextInput
                      style={styles.input}
                      placeholder="Usuário"
                      autoCorrect={false}
                      onChangeText={()=> {}}
                   />

                   <TextInput
                      style={styles.input}
                      placeholder="Senha"
                      autoCorrect={false}
                      secureTextEntry
                      onChangeText={()=> {}}
                   />

                   <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <TouchableOpacity style={styles.btnSubmit}>
                       <Button
                            type="clear"
                            icon={<Icon name='chevron-forward-circle-outline' type='ionicon' size={50} color='#373f4c' />}
                            onPress={() => this.props.navigation.push("Atendente")}
                           
                        
                                
                          />
                      </TouchableOpacity>
                   </View>


                   <TouchableOpacity style={styles.btnSenha}>
                       <Text style={styles.SenhaText}>Esqueceu Senha</Text>
                   </TouchableOpacity>
                   
               </View>
           </KeyboardAvoidingView>
        )
    }
}

export default Login;

const styles = StyleSheet.create({
    background:{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#eeeeee'
    },
    containerLogo:{
        flex:1,
        justifyContent: 'center'
    },
    container:{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '90%',
        paddingBottom: 50
    },

    input:{
        backgroundColor: '#ffffff',
        width: '90%',
        marginBottom: 15,
        color: '#222',
        fontSize: 17,
        borderRadius: 7,
        padding: 10,
    },

    btnSubmit:{
        backgroundColor: '#f1f1f1',
        width: '90%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7
    },

    submitText:{
        Color: '#fff',
        fontSize: 18
    },
    btnSenha:{
        marginTop: 10,

    },

    SenhaText:{
        color:'#07bdd5'
    }
});