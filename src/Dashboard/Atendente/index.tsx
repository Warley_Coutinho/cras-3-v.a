import { StatusBar } from 'expo-status-bar';

import React from 'react';

import { View, Text, StyleSheet, ScrollView, Image, TouchableOpacity } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons'
import { Button, Icon} from 'react-native-elements';


class Atendente extends React.Component{

    constructor(props, context){
        super(props, context);
    }
    onPressNew = () => {
        this.props.navigation.navigate('Formulario', { beneficiario: {} });
    }
    
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image
                        source={require('../../../assets/img/Banner.png')}
                        style={styles.image}
                    />
                    
                    <View style={styles.line} />
                    <View style={styles.textContainer}>
                       
                        <TouchableOpacity style={{ position: 'absolute', right: 0, alignSelf: "center" }}>
                            <MaterialIcons
                                name="search"
                                size={30}
                                color='#096058'
                            />
                        </TouchableOpacity>

               
                    </View>    
                </View>

                <View style={styles.line} />

                <ScrollView>
                    <Text style={styles.text}>Operações</Text>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>

                        <Button 
                             type="clear"
                             title = "Cadastro"
                            icon={<Icon name='person-add' type='ionicon' size={100} color='#096058' />}
                            onPress={() => this.onPressNew()}
                        />
                         <Button 
                             type="clear"
                             title = "Lista"
                            icon={<Icon name='list' type='ionicon' size={100} color='#096058' />}
                            onPress={() => this.props.navigation.push("Lista")}
                        />
                    </View>


                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>

                        <Button
                            type="clear"
                            title="Beneficio"
                            icon={<Icon name='card-outline' type='ionicon' size={100} color='#096058' />}
                            onPress={() => this.props.navigation.push("#")}

                        />

                        <Button
                            type="clear"
                            title="Relátorio"
                            icon={<Icon name='reader-outline' type='ionicon' size={100} color='#096058' />}
                            onPress={() => this.props.navigation.push("#")}
                        />


                    </View>
                </ScrollView>

                <View style={styles.line} />

                <View style={styles.textContainer}>

                </View>
                <Image style={styles.container}
                    source={require('../../../assets/img/Banner.png')}
                />

            </View>


        );
    }

}
export default Atendente;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: '#f1ecec',
    },

    header: {
        marginBottom: 100
    },
    image: {
        width: '100%',
        height: '100%'
    },

    textContainer: {
        flexDirection: 'row',
        marginVertical: '5%',
        marginHorizontal: '5%'
    },
    text: {

        fontSize: 26,
        marginHorizontal: '1%',
        color: '#5686d6'
    },
    
    line: {
        borderBottomColor: '#096058',
        borderBottomWidth: 2,
    }

});