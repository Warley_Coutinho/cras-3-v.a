import values from "lodash/values";
// mapStateToProps

export const pessoaFilter = ({pessoaState}) => {
    return {
        pessoaItem: pessoaState.pessoaItem,
        pessoaLista: values(pessoaState.pessoaLista)
    }
}