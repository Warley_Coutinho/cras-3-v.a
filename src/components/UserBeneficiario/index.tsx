import React from 'react';
import { SafeAreaView, StyleSheet, Image, Text, TouchableOpacity } from "react-native";
import { useNavigation } from '@react-navigation/native';

const UserItem = ({item}) => {

    const navigation = useNavigation();

    const onPress = () => { navigation.navigate('Detalhes',{id: item.id}) };

    return (
        <SafeAreaView style={styles.wrapper}>
            <TouchableOpacity style={styles.container} onPress={onPress} >
                <Image style={styles.userIcon} source={{ uri: item.img }} />
                <Text>{item.name}</Text>
            </TouchableOpacity>
        </SafeAreaView>
    );
};


const UserBeneficiario = ({item}) => {
    return (
        <SafeAreaView>
            <UserItem item={item} />
        </SafeAreaView>
    );
};

export default UserBeneficiario;
const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: "#5686d689",
        padding: 5,
        marginTop: 2

    },
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5

    },
    userIcon: {
        width: 40,
        height: 40,
        marginRight: 10
    }
});