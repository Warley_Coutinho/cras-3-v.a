import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


const Hr = (props) => {
    return (
        <View style={styles.hrContainer}>
            <View style={styles.hrLine} />
            <Text style={styles.hrText}> {props.text} </Text>
            <View style={styles.hrLine} />
        </View>
    );
}

export default Hr;


const styles = StyleSheet.create({
    hrContainer: {
        flexDirection: 'row', 
        alignItems: 'center'
    },
    hrLine: {
        flex: 1, 
        height: 1, 
        backgroundColor: 'black'
    },
    hrText: {
        textAlign: 'center', 
        paddingLeft: 5, 
        paddingRight:5, 
        fontSize: 18, 
        fontWeight: "bold"
    }

})