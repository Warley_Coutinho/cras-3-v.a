import axios from "axios";

const API_URL = "http://localhost:3000/";

const login = (auth) => {
    return axios.post(API_URL, auth)
        .then(response => {
            if (response.data.jwtToken) {
                localStorage.setItem("user", JSON.stringify(response.data))
            }
        })
}

const logout = () => {
    localStorage.removeItem("user");
}

const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem('user'));;
}

export default { login, logout, getCurrentUser }