import axios from 'axios'; 

class BeneficiaroService{

    connection = axios.create({baseURL: 'http://localhost:3000/'});


    findAll(){
        return this.connection.get('/Beneficiario');
    }

    findById( id ){
        return this.connection.get('/Beneficiario/'+id);
    }

    save( person ){

        if(person.id == undefined){
            return this.connection.post('/Beneficiario/', person)
        } 
        else {
            return this.connection.put('/Beneficiario/'+person.id, person)
        }

    }

    delete(id){
        return this.connection.delete('/Beneficiario/'+id);
    }

}

export default new BeneficiaroService();