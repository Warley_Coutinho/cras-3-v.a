# Comando para habilitar o terminal a executar scripts
Set-ExecutionPolicy Unrestricted

# Executar o Json Server
json-server --watch db.json

# Executar o projeto
npm start